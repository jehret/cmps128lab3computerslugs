import time
from flask import Flask
from flask import request
from flask import Response
import sys
import json
import os
import requests

app = Flask(__name__)
KVS = {}
vcKVS = {}
@app.route('/')
def standby():
        return 'standby'

view = (os.environ.get('VIEW').split(","))
ip_port = os.environ.get('IP_PORT')

@app.route("/keyValue-store/<key>", methods = ['PUT','GET','DELETE'])
def root(key):
        payload = json.loads(request.args.get('payload'))
        if request.method == 'PUT':
                value = request.args.get('val')
                if key is None:
                        data = ({'msg':'Error','error':'Key not valid'})
                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')

                if value is None:
                        data = ({'msg':'Error','error':'Value is missing'})
                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')

                if len(key) < 1 or len(key) >= 200 :
                        data = ({'msg':'Error','error':'Key not valid'})
                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')

                if sys.getsizeof(value)>1048576 :
                        data = ({'error':'Object too large. Size limit is 1MB','msg':'Error'})
                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')

                if key in KVS:
                        if key in payload:
                                if vcKVS[key] <= payload[key]:
                                        vcKVS[key] = payload[key] + 1
                                        # write to all containers
                                        for ipAdd in view:
                                                try:
                                                        if ipAdd != ip_port:
                                                                requests.put('http://'+ ipAdd + '/keyValue-store/'+key,data = request.form , timeout = 5)
                                                except requests.exceptions.RequestException as e:
                                                        print(e)
                                        KVS[key] = value
                                        data = {'replaced':True,'msg':'Updated successfully','payload':vcKVS}
                                        return Response(json.dumps(data), status = 200, mimetype = 'application/json')
                                else:
                                        data = {'result':'Error', 'msg':'Payload out of date','payload':vcKVS}
                                        return Response(json.dumps(data), status = 400, mimetype = 'application/json')
                        else:
                                data = {'result':'Error', 'msg':'Payload out of date','payload':vcKVS}
                                return Response(json.dumps(data), status = 400, mimetype = 'application/json')
                else:
                        if key in payload:
                                vcKVS[key] = payload[key] + 1
                                # write to all containers
                                for ipAdd in view:
                                        try:
                                                if ipAdd != ip_port:
                                                        requests.put('http://'+ ipAdd + '/keyValue-store/'+key,data = request.form , timeout = 5)
                                        except requests.exceptions.RequestException as e:
                                                print(e)
                        else:
                                vcKVS[key] = 1
                        KVS[key] = value
                        data = {'replaced':False, 'msg':'Added successfully','payload':vcKVS}
                        return Response(json.dumps(data), status = 201, mimetype = 'application/json')
        elif request.method == 'GET':
                if key in KVS:
                        if key in payload:
                                if vcKVS[key] <= payload[key]:
                                        data = {'msg':'Success', 'value':KVS[key],'payload':vcKVS}
                                        return Response(json.dumps(data), status = 200, mimetype = 'application/json')
                                else:
                                        data = {'result':'Error', 'msg':'Payload out of date','payload':vcKVS}
                                        return Response(json.dumps(data), status = 400, mimetype = 'application/json')
                        else:
                                data = {'result':'Error', 'msg':'Payload out of date','payload':vcKVS}
                                return Response(json.dumps(data), status = 400, mimetype = 'application/json')
                else:
                        data = {'msg': 'Error', 'error': 'Key does not exist','payload':vcKVS}
                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')
        elif request.method == 'DELETE':
                if key in KVS:
                        if key in payload:
                                if vcKVS[key] <= payload[key]:
                                        del vcKVS[key]
                                        del KVS[key]
                                        data = {'msg':'Success','payload':vcKVS}
                                        return Response(json.dumps(data), status = 200, mimetype = 'application/json')
                                else:
                                        data = {'msg': 'Error', 'error': 'Key does not exist','payload':vcKVS}
                                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')
                        else:
                                data = {'msg': 'Error', 'error': 'Key does not exist','payload':vcKVS}
                                return Response(json.dumps(data), status = 404, mimetype = 'application/json')
                else:
                        data = {'msg':'Error', 'error':'Key does not exist','payload':vcKVS}
                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')
        else:
                return 'end'

@app.route('/keyValue-store/search/<key>', methods = ['GET'])
def search(key):
        payload = json.loads(request.args.get('payload'))
        if request.method == 'GET':
                if key in KVS:
                        if key in payload:
                                if vcKVS[key] <= payload[key]:
                                        data = {'isExists': True, 'result': 'Success','payload':vcKVS}
                                        return Response(json.dumps(data), status = 200, mimetype = 'application/json')
                                else:
                                        data = {'msg':'Error', 'error':'Key does not exist','payload':vcKVS}
                                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')
                        else:
                                data = {'msg':'Error', 'error':'Key does not exist','payload':vcKVS}
                                return Response(json.dumps(data), status = 404, mimetype = 'application/json')
                else:
                        data = {'isExist': False, 'result':'Error','payload':vcKVS}
                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')
        return 'end'

"""
#given key, return value
@app.route('/keyValue-store/get/', methods=['GET'])
def get():
    if request.method == 'GET':
        key = request.args.get('key')

        if KVS[key]
"""




@app.route("/view", methods = ['PUT','GET','DELETE'])
def viewResp():
        ipport = request.form.get('ip_port')
        if requests.method == 'GET':
                retVal = ""
                count = 0
                # concatinate the view list into a string to be returned
                for ipVal in view:
                        count +=1
                        #only want commas in between values
                        if count < len(view) :
                                retVal += ipVal + ","
                        else:
                                retVal += ipVal
                data = {'view' : retVal}
                return Response(json.dumps(data), status = 200, mimetype = 'application/json')

        elif request.method == 'PUT':

                if ipport in view:
                        msg = ipport + " is already in view"
                        data = {'result': "Error", 'msg': msg}
                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')

                else:
                        view.append(ipport)
                        try:
                                for ipAdd in view:
                                        if ipAdd != ip_port:
                                                res = requests.put('http://'+ ipAdd + '/view/'+ipport,timeout = 5)
                                        msg = "Successfully added " + ipport + " to view"
                                        data = {'result': "Success", 'msg': msg}
                        except requests.exceptions.RequestException as e:
                                print(e)
                        return Response(json.dumps(data), status = 200, mimetype = 'application/json')



        elif request.method == 'DELETE':
                if ipport in view:
                        view.remove(ipport)
                        for ipAdd in view:
                                if ipAdd != ip_port:
                                        res = requests.delete('http://'+ ipAdd + '/view/'+ipport)
                        msg = "Successfully removed " + ipport + " from view"
                        data = {'result': "Success", 'msg': msg}
                        return Response(json.dumps(data), status = 200, mimetype = 'application/json')
                else:
                        msg = ipport + " is not in current view"
                        data = {'result': "Error", 'msg': msg}
                        return Response(json.dumps(data), status = 404, mimetype = 'application/json')


if __name__ == "__main__":
        app.run(host="0.0.0.0", port=8080, debug=True)
