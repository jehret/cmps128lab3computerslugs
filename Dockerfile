# Use an official Python runtime as a parent image
FROM python:3.7-slim

# Set the working directory to /webapp
WORKDIR /KVS

# Copy the current directory contents into the container at /webapp
COPY . /KVS

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Define environment variable
ENV NAME World

# Run webapp.py when the container launches
CMD ["python", "KVS.py"]

